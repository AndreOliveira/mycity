﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour {

	private Image fadeScreen;
	[SerializeField] private float transitionTime = 1f;

	void Start() {
		fadeScreen = GetComponentInChildren<Image> ();
		Vector3 position = Camera.main.transform.position;
		//put the blackscreen to fade in/out at cameras front
		position.z = Camera.main.transform.position.z + 1f;
		transform.position = position;
		Fade (false);
	}

	public void GameOver() {
		AudioManager.GetInstance.SetMixer (true);
		ChangeScene ("GameOver");
	}

	public void Restart () {
		AudioManager.GetInstance.SetMixer (false);
		ChangeScene ("StartMenu");
	}
		
	public void ChangeScene(string scene) {
		StartCoroutine (TransitionTo(scene));
	}
		
	public void Quit() {
		Application.Quit ();
	}
		
	private void Fade(bool fadeIn) {
		fadeScreen.enabled = true;
		fadeScreen.CrossFadeAlpha (fadeIn ? 1f : 0f, transitionTime, true);
	}


	private IEnumerator TransitionTo (string scene) {
		Fade (true);
		Time.timeScale = 1f;
		yield return new WaitForSeconds (transitionTime);
		SceneManager.LoadScene (scene);
	}

}