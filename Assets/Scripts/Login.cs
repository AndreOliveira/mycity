﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour {

	[SerializeField] private InputField inputField;
	[SerializeField] private Text errorMessage;
	[SerializeField] private MySceneManager sceneManager;
	[SerializeField] private int startMoney = 250;

	public void PlayGame() {
		if (!string.IsNullOrEmpty(inputField.text)) {
			PlayerPrefs.SetString("Name",inputField.text);
			PlayerPrefs.SetInt("Money",startMoney);
			sceneManager.ChangeScene("Gameplay");
		} else {
			errorMessage.gameObject.SetActive(true);
		}
	}
	
}
