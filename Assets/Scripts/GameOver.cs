﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

	[SerializeField] private Text yourScore;
	[SerializeField] private Text highScore;
	[SerializeField] private ParticleSystem newRecord;

	void Start () {
		highScore.text = PlayerPrefs.GetInt ("HighScore").ToString();
		yourScore.text = PlayerPrefs.GetInt ("YourScore").ToString();

		if (PlayerPrefs.GetInt ("NewRecord") == 1) {
			newRecord.Play ();
		} else {
			PlayerPrefs.SetInt ("NewRecord", 0);
		}
	}
}