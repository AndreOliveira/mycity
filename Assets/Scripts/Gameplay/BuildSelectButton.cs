﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildSelectButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	#region fields
	private Selectable selectable;
	private Rigidbody2D newBuildsRB;
	private SpriteRenderer spriteR;
	private Animator animator;
	private bool alreadyIsInteractable;
	[SerializeField] private ScriptableBuild build;
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private Text description;
	[Range(0,5)] [SerializeField] private int offsetY;
	#endregion //fields

	#region unity messages
	void Start () {
		alreadyIsInteractable = false;
		animator = GetComponent<Animator> ();
		selectable = GetComponent<Selectable> ();
		GameManager.GetInstance.buildButtons.Add (this);
		CheckEconomy ();
		description.text = "Price: " + build.price.ToString ();
	}

	//when the player touch in this object, this method will be called and will
	//verify if its object is interactable (if the player have money to buy it)
	// if is interactable, the setup preparing the new build will be done...
	//and may the player will chose the place to start the build
	public void OnPointerDown (PointerEventData data) {
		if (selectable.interactable) {
			GameManager.GetInstance.isDraggingBuilding = true;
			Vector3 position = this.transform.position;
			position.z = 0f;
			Vector2 snappedPosition = new Vector2 (position.x, position.y);
			SnapInputPosition (ref snappedPosition);
			GameObject newBuild = GameManager.GetInstance.buildingPool.Spawn (snappedPosition);
			newBuildsRB = newBuild.GetComponent<Rigidbody2D> ();
			spriteR = newBuild.GetComponent<SpriteRenderer> ();
			spriteR.sprite = build.sprite;
			spriteR.color = new Color (1f, 1f, 1f, 0.25f);
			GameManager.GetInstance.camController.canUseZoom = false;
		} 
	}

	//verify if is possible to create a new build in the place
	//where the players stop to touch the screen, if it is possible
	//then start the construction, otherwise the construction is aborted
	public void OnPointerUp (PointerEventData data) {
		GameManager.GetInstance.isDraggingBuilding = false;

		if (newBuildsRB != null) {
			spriteR.color = Color.white;
			Build newBuild = newBuildsRB.GetComponent<Build> ();
			if (newBuild.wrongPlaces <= 0) {
				newBuild.build = build;
				GameManager.GetInstance.Money -= build.price;
				newBuild.StartBuild ();
				//newBuildsRB.GetComponent<Animator> ().enabled = true;
				newBuildsRB = null;
				spriteR.sortingOrder = (int) -newBuild.transform.position.y;
				GameManager.GetInstance.camController.canUseZoom = true;
			} else {
				newBuild.gameObject.SetActive (false);
			}
		}
	}

	//will move the possible new build while the player drag the finger (touching) over screen and snap the position
	void FixedUpdate() {
		Vector2 inputPosition;
		if (newBuildsRB != null && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			inputPosition = (Vector2) Camera.main.ScreenToWorldPoint (new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y));
			inputPosition.y += offsetY;
			SnapInputPosition(ref inputPosition);
			newBuildsRB.MovePosition (inputPosition);
		}

	}//end of fixed update

	#endregion //unity messages

	#region my Methods
	//check if the player has money to buy this build and let it interactable or not
	//this method will be called everty time the money is updated (on set method)
	public void CheckEconomy() {
		switch (build.type) {
		case BuildType.FACTORY:
			selectable.interactable = GameManager.GetInstance.factory.price <= GameManager.GetInstance.Money;
			break;
		case BuildType.FARM :
			selectable.interactable = GameManager.GetInstance.farm.price <= GameManager.GetInstance.Money;
			break;
		case BuildType.HOUSES :
			selectable.interactable = GameManager.GetInstance.houses.price <= GameManager.GetInstance.Money;
			break;
		case BuildType.MALL:
			selectable.interactable = GameManager.GetInstance.mall.price <= GameManager.GetInstance.Money;
			break;
		case BuildType.PARK :
			selectable.interactable = GameManager.GetInstance.park.price <= GameManager.GetInstance.Money;
			break;
		}
		if (selectable.interactable && !alreadyIsInteractable) {
			animator.SetTrigger ("BeReady");
			audioSource.PlayOneShot (audioSource.clip);
			alreadyIsInteractable = true;
		}
		if (!selectable.interactable && alreadyIsInteractable) {
			alreadyIsInteractable = false;
		}
	}//end CheckIfCanBeBuilt

	public bool Interactable {
		get { return selectable.interactable; }
		set {
			selectable.interactable = value;
		}
	}//end CanBeBuilt (get-set)

	private void SnapInputPosition(ref Vector2 inputPosition)  {
		//snap the x value into multiples of three
		float x = Mathf.Round (inputPosition.x);
		x = (Mathf.Abs(x) % 3 == 0) ? x : x + ((x > 0) ? 1 : -1) * ((Mathf.Abs(x) % 3 == 1) ? - 1 : 1);

		//snap the y value into multiples of two
		float y = Mathf.Round (inputPosition.y);
		y = Mathf.Abs(y) % 2 == 0 ? y : y + 1;

		inputPosition.x = x;
		inputPosition.y = y;
	}

	#endregion //my methods
}//end of class
