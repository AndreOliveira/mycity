﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {

	[SerializeField] private Sprite playIcon;
	[SerializeField] private Sprite pauseIcon;
	[SerializeField] private GameObject pauseMenu;

	public void PauseUnpause(UnityEngine.UI.Image image) {
		Time.timeScale = Time.timeScale == 1f ? 0f : 1f;
		pauseMenu.SetActive (Time.timeScale == 0f);
		image.sprite = Time.timeScale == 1f ? pauseIcon : playIcon;
		AudioListener.pause = Time.timeScale == 0f;
	}


}
