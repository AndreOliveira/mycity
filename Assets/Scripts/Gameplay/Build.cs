﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator),typeof(SpriteRenderer))]
public class Build : MonoBehaviour {

	#region FIELDS
	public ScriptableBuild build;
	public int wrongPlaces { get; private set; }

	[SerializeField] private Image clock;
	[SerializeField] private AudioClip buildingClip;
	[SerializeField] private AudioClip finishingBuildingClip;
	[SerializeField] private Canvas canvas;
	[SerializeField] private ParticleSystem smoke;
	[SerializeField] private ParticleSystem stars;

	private AudioSource audioSource;
	private SpriteRenderer spriteR;
	private Animator animator;
	private bool isChoosingPlace;

	#endregion //end of fields region

	#region Unity Messages
	void Awake() {
		animator = GetComponent<Animator> ();
		spriteR = GetComponent<SpriteRenderer> ();
		canvas.worldCamera = Camera.main;
		audioSource = GetComponent<AudioSource> ();
	}

	void OnEnable () {
		smoke.Stop ();
		isChoosingPlace = true;
		clock.enabled = false;
		animator.enabled = false;
		wrongPlaces = 0;
	}

	#endregion //end of unity messages

	#region methods
	public void StartBuild() {
		audioSource.Play ();
		animator.enabled = true;
		isChoosingPlace = false;
		clock.enabled = true;
		clock.transform.position = transform.position;
		StartCoroutine (Building(build.buildTime));
	}

	IEnumerator Building (float timer) {
		smoke.Play ();
		float startTime = timer;
		clock.fillAmount = 1f;
		animator.SetTrigger ("StartBuild");
		while (clock.fillAmount > 0.01f) {
			timer -= Time.fixedDeltaTime;
			clock.fillAmount = timer / startTime;
			clock.transform.position = transform.position;
			yield return new WaitForFixedUpdate ();
		}
		clock.fillAmount = 0f;
		animator.enabled = false;
		spriteR.sprite = build.sprite;
		StartCoroutine (Working ());
		audioSource.Stop ();
		audioSource.PlayOneShot (finishingBuildingClip);
	}

	IEnumerator Working() {
		GenerateResources (true);
		smoke.Stop ();
		stars.Play ();
		while (true) {
			yield return new WaitForSeconds (build.periodicity);
			GenerateResources (false);
		}
	}

	//generate resources based if the building was builded right now or if is periodic resource generation (startersResources parameter)
	public void GenerateResources(bool startersResources) {
		if (startersResources && build.startIncome > 0 || !startersResources && build.income != 0) {
			GameManager.GetInstance.coinsManager.RequestCoins (startersResources ? build.startIncome : build.income, transform.position);
		}
		if (startersResources && build.startFood > 0 || !startersResources && build.food != 0) {
			GameManager.GetInstance.coinsManager.RequestFood (startersResources ? build.startFood : build.food, transform.position);
		}
		if (startersResources && build.startLifeQuality > 0 || !startersResources && build.lifeQuality != 0) {
			GameManager.GetInstance.coinsManager.RequestLifeQuality (startersResources ? build.startLifeQuality : build.lifeQuality, transform.position);
		}
		if (startersResources && build.startPeople > 0 || !startersResources && build.people != 0) {
			GameManager.GetInstance.coinsManager.RequestPeople (startersResources ? build.startPeople : build.people, transform.position);
		}
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if (coll.CompareTag ("Grass") && !isChoosingPlace) {
			Destroy(coll.gameObject);
		}

		if (coll.CompareTag ("Building") && isChoosingPlace) {
			spriteR.color = new Color (.25f,.25f,.25f,0.25f);
			wrongPlaces++;
		}
	}

	void OnTriggerExit2D (Collider2D coll) {
		if (coll.CompareTag ("Building") && isChoosingPlace) {
			wrongPlaces--;
			if (wrongPlaces <= 0) {
				spriteR.color = Color.white;
			}
		}
	}

	#endregion //end methods region
}
