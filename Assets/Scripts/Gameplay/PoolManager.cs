﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

	[SerializeField] private int amount;
	[SerializeField] private bool escalable;
	public GameObject pooledObject;
	private List<GameObject> pool;


	// Use this for initialization
	void Start () {
		pool = new List<GameObject> ();
		for (int i = 0; i < amount; i++) {
			AddPooledObject ();
		}
	}
	
	// if the object is inactive, then spawn...
	//else, verify if can create more (escalable) objects
	//if you can, create, else, return null
	public GameObject Spawn (Vector3 position) {
		for (int i = 0; i < pool.Count; i++) {
			if (!pool [i].activeInHierarchy) {
				pool [i].transform.position = position;
				pool [i].SetActive (true);
				return pool[i];
			}
		}//end for
		if (escalable || pool.Count < amount) {
			pool.Add (Instantiate (pooledObject,position,Quaternion.identity,transform) as GameObject);
			return pool[pool.Count-1];
		}
		return null;
	}//end spawn

	private void AddPooledObject() {
		pool.Add (Instantiate (pooledObject,transform));
		pool [pool.Count-1].SetActive (false);
	}
}
