﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : MonoBehaviour {

	[SerializeField] PoolManager coinsPool, superCoinsPool, megaCoinsPool;
	[SerializeField] PoolManager foodPool, peoplePool, lifeQualityPool;

	private int coinValue, superCoinValue,megaCoinValue;


	void Start() {
		coinValue = coinsPool.pooledObject.GetComponent<Collectable> ().Value;
		superCoinValue = superCoinsPool.pooledObject.GetComponent<Collectable> ().Value;
		megaCoinValue = megaCoinsPool.pooledObject.GetComponent<Collectable> ().Value;
	}

	//this will request coins, if is a high value, then the mega coin will be spawned
	//until the amount be lower then mega coin value... then...
	//the process will be repeated with super coin, and after that with coin (normal ones)
	//if some of that step isn't needed, it will be ignored
	public void RequestCoins (int amount, Vector3 position) {
		for (; amount >= megaCoinValue; amount -= megaCoinValue) {
			if (megaCoinsPool.Spawn (position) == null)
				Collectable.InstantCollect (megaCoinValue,CollectableType.COIN);
		}

		for (; amount >= superCoinValue; amount -= superCoinValue) {
			if (superCoinsPool.Spawn (position) == null)
				Collectable.InstantCollect (superCoinValue,CollectableType.COIN);
		}

		for (; amount >= coinValue; amount -= coinValue) {
			if (coinsPool.Spawn (position) == null)
				Collectable.InstantCollect (coinValue,CollectableType.COIN);
		}
	}

	public void RequestFood (int amount, Vector3 position) {
		GameObject food = foodPool.Spawn (position);
		if (food == null) {
			Collectable.InstantCollect (amount, CollectableType.FOOD);
		} else {
			food.GetComponent<Collectable> ().Value = amount;
		}
	}

	public void RequestPeople (int amount, Vector3 position) {
		GameObject people = peoplePool.Spawn (position);
		if (people == null) {
			Collectable.InstantCollect (amount, CollectableType.PEOPLE);
		} else {
			people.GetComponent<Collectable> ().Value = amount;
		}
	}

	public void RequestLifeQuality (int amount, Vector3 position) {
		GameObject lifeQuality = lifeQualityPool.Spawn (position);
		if (lifeQuality == null) {
			Collectable.InstantCollect (amount, CollectableType.FOOD);
		} else {
			lifeQuality.GetComponent<Collectable> ().Value = amount;
		}
	}
}
