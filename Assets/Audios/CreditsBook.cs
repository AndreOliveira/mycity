/*
*******************************AUDIO***********************************


SOUNDTRACK (ST)

Intro/Credits			http://freemusicarchive.org/music/Komiku/THE_GIRL_WITH_THE_BASEBALL_BAT/Komiku_-_THE_GIRL_WITH_THE_BASEBALL_BAT_-_03_In_the_restaurant
City (Gameplay)			http://freemusicarchive.org/music/Komiku/ULTRA_PERSON_VOL3/Komiku_-_ULTRA_PERSON_VOL3_-_04_Shopping_List

SOUNDS EFFECTS (SFX)

FinishBuild				https://freesound.org/people/plasterbrain/sounds/397355/
ThrowCoins				https://freesound.org/people/Agaxly/sounds/233615/
Coin					https://freesound.org/people/sharesynth/sounds/344520/
Click					https://freesound.org/people/ecfike/sounds/128919/
EnableBuilding			https://freesound.org/people/relwin/sounds/171064/
Building				http://freesound.org/people/VKProduktion/sounds/235631/






********************************ARTTS*************************************
*
Grass2					https://kenney.nl/assets/foliage-pack
Meat icon 				https://publicdomainvectors.org/en/free-clipart/Ham-icon-vector-image/18406.html
People Icon				https://kenney.nl/assets/game-icons
*/